APLICACIÓN GOOGLE MAPS

CONFIGURANDO FIREBASE

1. Crear un nuevo proyecto en Firebase.

https://console.firebase.google.com/?hl=es-419

Ir a la consola de firebase. Acceder con su cuenta de google:



2. Click en el proyecto.
3. Seleccionar en el menu lateral la opción DATABASE.
4. En el panel de DATOS, Importar el archivo departamentos.JSON




5. En el panel de REGLAS, pegar el siguiente contenido:
```
{
  "rules": {
    ".read": "auth == null",
    ".write": "auth == null"
  }
}
```


6. Ahora seleccionamos del menú lateral la opción OVERVIEW
7. Click en el boton Agregar Firebase a tu aplicación web





Copiamos el código en el archivo index.html
```
<script src="https://www.gstatic.com/firebasejs/3.6.4/firebase.js"></script>
<script>
// Initialize Firebase
var config = {
  apiKey: "AIzaSyBmDZRPROtakfFU0M-jSWcii2NfPAV-ZEU",
  authDomain: "project-7173459743270241243.firebaseapp.com",
  databaseURL: "https://project-7173459743270241243.firebaseio.com",
  storageBucket: "project-7173459743270241243.appspot.com",
  messagingSenderId: "1067805660048"
};
firebase.initializeApp(config);
</script>
```
6. Listo, ya se puede acceder a la base de datos de firebase.








CONFIGURANDO GOOGLE MAPS

1. Crear una API KEY de javascript para usar Google maps

https://developers.google.com/maps/documentation/javascript/get-api-key



2. Modificar la siguiente línea del archivo index.html

<script async defer src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap" type="text/javascript"></script>

donde dice YOUR_API_KEY, colocar la API KEY Generada.Debera quedar de la siguiente forma:

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApXsqxYy_jwsfQgwBcC8XwVa02f055QzY&callback=initMap"></script>

3. Listo ya podemos crear la etiqueta: <div id="map"></div> en el archivo index.html, que es donde aparecerá el mapa.

NOTAS.-
Los datos se actualizan automáticamente cuando se modifica la base de datos, no es necesario recargar la página.


REFERENCIAS:

Agregar Firebase a tu proyecto web:
https://firebase.google.com/docs/web/setup?hl=es-419

Collaborative Realtime Mapping with Firebase:
https://developers.google.com/maps/documentation/javascript/firebase

Primeros pasos (API de Google Maps Para Web Maps JavaScript API):
https://developers.google.com/maps/documentation/javascript/tutorial


Ejemplo:
http://angular121.hol.es/mapas-google/index.html
